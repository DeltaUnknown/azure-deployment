#!/bin/bash

# Fetches the hosts public ssh key and adds it to the known hosts.
ssh-keyscan -p "$port" "$host" >> ~/.ssh/known_hosts

# Update package lists
sudo apt-get update 

#Install required packages
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common gnupg-agent

# Add Docker GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Add Docker repository
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu focal stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Update package lists w/ Docker repository
sudo apt-get update

# Install Docker & Docker Compose
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Add the current user to the group "docker", to run commands without docker
sudo usermod -aG docker $USER

# Enable Docker to start up on system boot
sudo systemctl enable docker

# Install Docker Compose
sudo curl -fsSL https://github.com/docker/compose/releases/latest/download/docker-compose-Linux-x86_64 -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Acquire required files
sudo apt-get install wget
sudo wget https://gitlab.com/DeltaUnknown/azure-deployment/-/raw/main/docker-compose.yml
sudo wget https://gitlab.com/DeltaUnknown/azure-deployment/-/raw/main/logstash.conf
sudo wget https://gitlab.com/DeltaUnknown/azure-deployment/-/raw/main/filebeat.yml



#Creat a docker file directory and prepare the file for use
sudo mkdir /home/adminuser/docker
mv docker-compose.yml /home/adminuser/docker/

#logstash
sudo mkdir -p /usr/share/logstash/ /usr/share/logstash/pipeline/
sudo mv logstash.conf /home/adminuser/docker/
sudo cp -a /home/adminuser/docker/logstash.conf /usr/share/logstash/pipeline/


#filebeat
sudo mkdir /usr/share/filebeat/
sudo mv filebeat.yml /home/adminuser/docker/
sudo cp -a /home/adminuser/docker/filebeat.yml /usr/share/filebeat/
sudo chmod -R 755 /var/log/


#Run docker-compose.yml
sudo chmod +x /home/adminuser/docker/docker-compose.yml
cd /home/adminuser/docker
sudo docker-compose -f /home/adminuser/docker/docker-compose.yml up -d