# Azure Provider source & Version
# https://gitlab.com/DeltaUnknown/azure-deployment/-/tree/main
# Essentieel
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configuratie van Microsoft Azure Provider 
# Essentieel
provider "azurerm" {
  skip_provider_registration = "true"
  features {}
}

# Creëert een  Resource groep
resource "azurerm_resource_group" "tf-rg" {
  name     = "tf-resources"
  location = "West Europe"
  tags = {
    environment = "test"
  }
}

# Creëert een Virtual Network
resource "azurerm_virtual_network" "tf-vn" {
  name                = "tf-virtual-network"
  resource_group_name = azurerm_resource_group.tf-rg.name
  location            = azurerm_resource_group.tf-rg.location
  address_space       = ["192.168.0.0/16"]

  tags = {
    environment = "test"
  }
}

# Creëert een Subnet
resource "azurerm_subnet" "tf-subnet" {
  name                 = "tf-subnet"
  resource_group_name  = azurerm_resource_group.tf-rg.name
  virtual_network_name = azurerm_virtual_network.tf-vn.name
  address_prefixes     = ["192.168.10.0/24"]
}

# Creëert een Security Group
resource "azurerm_network_security_group" "tf-sg" {
  name                = "tf-sg"
  resource_group_name = azurerm_resource_group.tf-rg.name
  location            = azurerm_resource_group.tf-rg.location

  # Custom Security Rules voor de Security Group
  security_rule {
    name                       = "test123"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "test"
  }
}

# Security Group koppelen aan Resource Group
resource "azurerm_subnet_network_security_group_association" "tf-sg" {
  subnet_id                 = azurerm_subnet.tf-subnet.id
  network_security_group_id = azurerm_network_security_group.tf-sg.id
}

# Creëert een Public IP Adres
resource "azurerm_public_ip" "tf-ip-01" {
  name                = "tf-ip-01"
  resource_group_name = azurerm_resource_group.tf-rg.name
  location            = azurerm_resource_group.tf-rg.location
  allocation_method   = "Dynamic"
}

# Creëert een Network Interface
resource "azurerm_network_interface" "tf-nic" {
  name                = "tf-nic"
  resource_group_name = azurerm_resource_group.tf-rg.name
  location            = azurerm_resource_group.tf-rg.location

  # Koppelt het Subnet en Public IP aan de NIC
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.tf-subnet.id
    public_ip_address_id          = azurerm_public_ip.tf-ip-01.id
    private_ip_address_allocation = "Dynamic"
  }
}

# Creëert een Virtual Machine (Linux)
# Voegt het toe aan de juiste resource group en network interface
resource "azurerm_linux_virtual_machine" "tf-lnx-01" {
  name                  = "LinuxMachine01"
  resource_group_name   = azurerm_resource_group.tf-rg.name
  location              = azurerm_resource_group.tf-rg.location
  size                  = "Standard_B2s"
  admin_username        = "adminuser"
  network_interface_ids = [azurerm_network_interface.tf-nic.id]

  # Roept de "customdata.tpl" op en execute het
  custom_data = filebase64("customdata.tpl")
  # Koppelt de SSH key aan de admin
  admin_ssh_key {
    username   = "adminuser"
    public_key = file("~/.ssh/tf_azurekey.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }
}

# Output het IP adres van de linux machine
data "azurerm_public_ip" "tf-lnx-01" {
  name                = azurerm_public_ip.tf-ip-01.name
  resource_group_name = azurerm_resource_group.tf-rg.name
}
output "public_ip_address" {
  value = "${azurerm_linux_virtual_machine.tf-lnx-01.name}: ${azurerm_public_ip.tf-ip-01.ip_address}"
}

# Mag maar een keer ge-execute worden
## Configuratie voor Azure Monitor
#resource "azurerm_monitor_diagnostic_setting" "tf-ms" {
#  name                       = "MonitorSettings"
#  target_resource_id         = azurerm_linux_virtual_machine.tf-lnx-01.id
#  storage_account_id         = azurerm_storage_account.tf-sa.id
#  log_analytics_workspace_id = azurerm_log_analytics_workspace.tf-la.id
#
#  # category werkt niet,   
#  #  log{
#  #    category = "AllLogs"
#  #    enabled = true
#  #  }
#
#  metric {
#    category = "AllMetrics" # Enabled de verzameling van alle beschikbare metrics
#    enabled  = true
#
#    retention_policy {
#      enabled = false
#    }
#  }
#}

# Configuratie voor  Log Analytics workspace
resource "azurerm_log_analytics_workspace" "tf-la" {
  name                = "LogAnalytics"
  location            = azurerm_resource_group.tf-rg.location
  resource_group_name = azurerm_resource_group.tf-rg.name
  sku                 = "PerGB2018" # Specificeerd de Log Analytics workspace SKU, PerGB2018 is het goedkoopste en goed genoeg voor tests.
  retention_in_days   = 30          # hoelang de data opgeslagen blijft, de free variant doet maar maximaal 7 dagen.
}

# Configuratie voor Storage Account
resource "azurerm_storage_account" "tf-sa" {
  name                     = "tfsa2405" # Moet uniek zijn over heel Azure
  resource_group_name      = azurerm_resource_group.tf-rg.name
  location                 = azurerm_resource_group.tf-rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS" # LRS is het goedkoopste, in ene echte enviroment waar up-time vereist is zou ik ZRS aanraden.
}

# Configureerd een Action Group
resource "azurerm_monitor_action_group" "tf-acg" {
  name                = "cpu actiongroup"
  resource_group_name = azurerm_resource_group.tf-rg.name
  short_name          = "act"
  enabled             = true

  email_receiver {
    name                    = "EmailRecipiant"
    email_address           = "459365@student.fontys.nl"
    use_common_alert_schema = true
  }

}

# Configureerd een Metric Alert
resource "azurerm_monitor_metric_alert" "tf-ma" {
  name                = "LNXAlertCPU"
  resource_group_name = azurerm_resource_group.tf-rg.name
  scopes              = [azurerm_linux_virtual_machine.tf-lnx-01.id]

  criteria {
    metric_namespace = "Microsoft.Compute/virtualMachines"
    metric_name      = "Percentage CPU"
    aggregation      = "Average"
    operator         = "GreaterThan"
    threshold        = 75
  }

  action {
    action_group_id = azurerm_monitor_action_group.tf-acg.id
  }
}